#-------------------------------------------------
#
# Project created by QtCreator 2014-05-08T11:35:28
#
#-------------------------------------------------

QT       += core concurrent gui multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = tapemachine
TEMPLATE = app

# win32: RC_ICONS = tape.ico


SOURCES += src/main.cpp \
           src/MainWindow.cpp \
           src/MarkerModel.cpp \
           src/AudioEncoder.cpp \
           src/SettingsDialog.cpp \
           src/LevelMeter.cpp \
           src/Texts.cpp

HEADERS += src/MainWindow.hpp \
           src/MarkerModel.hpp \
           src/AudioEncoder.hpp \
           src/Marker.hpp \
           src/SettingsDialog.hpp \
           src/LevelMeter.hpp \
           src/Texts.hpp \
           src/Version.hpp

FORMS   += ui/MainWindow.ui \
           ui/SettingsDialog.ui

QMAKE_CXXFLAGS += -std=c++11
INCLUDEPATH += src/
DEFINES += "TAPEMACHINE_VERSION=\\\"$(shell git -C \""$$_PRO_FILE_PWD_"\" describe --tags --always)\\\""

LIBS += -lavcodec -lavformat -lavutil

RESOURCES += resources/resources.qrc
