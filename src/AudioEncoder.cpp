/*
  Copyright (C) 2019  Johannes Oertel

  This program is free software: you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free Software
  Foundation, either version 3 of the License, or (at your option) any later
  version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
  details.

  You should have received a copy of the GNU General Public License along with
  this program. If not, see <https://www.gnu.org/licenses/>.
*/

#include "AudioEncoder.hpp"

#include <stdexcept>
#include <mutex>
#include <vector>

extern "C"
{
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
}

#include <QDebug>
#include <QFile>

struct AudioEncoder::impl
{
    impl()
        : oc(nullptr, [](AVFormatContext* p) {if(p) { avformat_free_context(p); } } ),
          stream(nullptr, [](AVStream* p) {if(p) { avcodec_close(p->codec); } } ),
          interleaved(false),
          use_markers(false)
    {
    }

    // Methods
    int checkSampleFormat(AVCodec *codec, enum AVSampleFormat sample_fmt)
    {
        const enum AVSampleFormat *p = codec->sample_fmts;

        while (*p != AV_SAMPLE_FMT_NONE) {
            if (*p == sample_fmt)
                return 1;
            ++p;
        }
        return 0;
    }

    // Variables
    std::once_flag flag;
    AVCodec* codec;
    std::unique_ptr<AVFormatContext, void(*)(AVFormatContext*)> oc;
    std::unique_ptr<AVStream, void(*)(AVStream*)> stream;
    AVCodecContext* ctx;
    bool interleaved;
    bool use_markers;
    std::vector<Marker>::const_iterator markersBegin, markersEnd;
};

AudioEncoder::AudioEncoder(QObject* parent, const QString& formatName)
    : QObject(parent),
      pimpl(new impl)
{
    // Initialize libavcodec and libavformat
    std::call_once(pimpl->flag, av_register_all);
    std::call_once(pimpl->flag, avcodec_register_all);

    // Try to allocate the output media context
    {
        AVFormatContext* tmp_oc;
        avformat_alloc_output_context2(&tmp_oc, NULL, qPrintable(formatName), NULL);
        pimpl->oc.reset(tmp_oc);
    }
    if(!pimpl->oc)
    {
        throw std::runtime_error("Could not find output format!");
    }

    // Does the selected format support an audio stream?
    if(pimpl->oc->oformat->audio_codec == AV_CODEC_ID_NONE)
    {
        throw std::runtime_error("Selected format doesn't support audio streams!");
    }

    // Try to find suitable encoder
    pimpl->codec = avcodec_find_encoder(pimpl->oc->oformat->audio_codec);
    if(!pimpl->codec)
    {
        throw std::runtime_error("Could not find audio codec!");
    }

    // Check if selected codec is an audio codec
    if(pimpl->codec->type != AVMEDIA_TYPE_AUDIO)
    {
        throw std::runtime_error("Selected codec isn't an audio codec!");
    }

    // Try to create audio stream
    pimpl->stream.reset(avformat_new_stream(pimpl->oc.get(), pimpl->codec));
    if(!pimpl->stream)
    {
        throw std::runtime_error("Could not create audio stream!");
    }
    pimpl->stream->id = pimpl->oc->nb_streams - 1;
    pimpl->ctx = pimpl->stream->codec;

    // Use signed int, 16 bit format, either planar or interleaved
    pimpl->ctx->sample_fmt = AV_SAMPLE_FMT_S16P;
    if(!pimpl->checkSampleFormat(pimpl->codec, pimpl->ctx->sample_fmt))
    {
        pimpl->ctx->sample_fmt = AV_SAMPLE_FMT_S16;
        if(!pimpl->checkSampleFormat(pimpl->codec, pimpl->ctx->sample_fmt))
        {
            throw std::runtime_error("Encoder doesn't support sample format!");
        }
        pimpl->interleaved = true;
    }

    // Check if format wants separate stream headers
    if(pimpl->oc->oformat->flags & AVFMT_GLOBALHEADER)
    {
        pimpl->ctx->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;
    }
}

AudioEncoder::~AudioEncoder() = default;

void AudioEncoder::encode(const QString& rawFileName, const QString& outFileName,
                          int sampleRate, int channels)
{
    // Open raw file
    QFile rawFile(rawFileName);
    rawFile.open(QIODevice::ReadOnly);
    qint64 bytesToRead = 0;

    // Should we use markers?
    if(!pimpl->use_markers)
    {
        // No, use whole file
        bytesToRead = rawFile.size();
    }
    else
    {
        // Yes, calculate size of selected parts
        for(auto i = pimpl->markersBegin, end = pimpl->markersEnd; i != end; ++i)
        {
            bytesToRead += i->end - i->begin;
        }
    }

    // Is there anything to encode?
    if(bytesToRead == 0)
    {
        return;
    }

    // Reset stream if used before
    pimpl->stream->cur_dts = 0;

    // Set maximum and minimum of attached QProgressBars
    emit minProgress(0);
    emit maxProgress(bytesToRead);

    // Set several parameters for the encoding
    pimpl->ctx->sample_rate = sampleRate;
    pimpl->ctx->channels = channels;
    pimpl->ctx->channel_layout = av_get_default_channel_layout(channels);

    // Try to open the encoder
    if(avcodec_open2(pimpl->ctx, pimpl->codec, NULL) < 0)
    {
        throw std::runtime_error("Could not open encoder!");
    }

    // Try to allocate frame containing raw input data
    std::unique_ptr<AVFrame, void(*)(AVFrame*)> frame(av_frame_alloc(), [](AVFrame* p) {av_frame_free(&p);} );
    if(!frame)
    {
        throw std::runtime_error("Could not initialise AVFrame!");
    }

    // Set parameters of frame to match those of the encoder
    frame->nb_samples = pimpl->ctx->frame_size;
    frame->format = pimpl->ctx->sample_fmt;
    frame->channel_layout = pimpl->ctx->channel_layout;

    // Try to calculate the buffer size needed for our encoding
    int buffer_size = av_samples_get_buffer_size(NULL, pimpl->ctx->channels,
                                                 pimpl->ctx->frame_size, pimpl->ctx->sample_fmt, 0);
    if(buffer_size < 0)
    {
        throw std::runtime_error("Could not get sample buffer size!");
    }
    int bytes_per_sample = av_get_bytes_per_sample(pimpl->ctx->sample_fmt);
    if(!bytes_per_sample)
    {
        throw std::runtime_error("Could not get bytes per sample for given format");
    }

    // Allocate memory for the input samples
    std::vector<std::uint16_t> samples(buffer_size/bytes_per_sample);

    // Try to connect samples buffer and AVFrame
    if(avcodec_fill_audio_frame(frame.get(), pimpl->ctx->channels, pimpl->ctx->sample_fmt,
                                reinterpret_cast<uint8_t*>(samples.data()), buffer_size, 0) < 0)
    {
        throw std::runtime_error("Could not fill AVFrame!");
    }

    // Open output file
    if(!(pimpl->oc->oformat->flags & AVFMT_NOFILE))
    {
        if(avio_open(&pimpl->oc->pb, qPrintable(outFileName), AVIO_FLAG_WRITE) < 0)
        {
            throw std::runtime_error("Could not open output file!");
        }
    }

    // Write the stream header
    if(avformat_write_header(pimpl->oc.get(), NULL) < 0)
    {
        if(!(pimpl->oc->oformat->flags & AVFMT_NOFILE))
        {
            avio_close(pimpl->oc->pb);
        }
        throw std::runtime_error("Could not write stream header!");
    }

    std::unique_ptr<AVPacket, void(*)(AVPacket*)> pkt(new AVPacket, av_free_packet);
    int got_output, i;

    // Seek to first marker if using them
    auto markerIt = pimpl->markersBegin;
    if(pimpl->use_markers)
    {
        rawFile.seek(markerIt->begin);
    }

    // Start encoding progress
    emit signalProgress(0);
    qint64 bytesLeft = bytesToRead;
    int samples_count = 0;
    bool flush_output = false, audio_eof = false;
    while(pimpl->stream && !audio_eof)
    {
        if(!flush_output && (!pimpl->stream || bytesLeft <= 0))
        {
            flush_output = true;
        }

        // Initialize output packet
        av_init_packet(pkt.get());
        pkt->data = NULL;
        pkt->size = 0;

        // Copy one frame of raw data to samples buffer
        for(i = 0; i < pimpl->ctx->frame_size; ++i)
        {
            if(pimpl->use_markers && rawFile.pos() >= markerIt->end)
            {
                if(++markerIt != pimpl->markersEnd)
                {
                    rawFile.seek(markerIt->begin);
                }
                else
                {
                    break;
                }
            }
            if(pimpl->interleaved)
            {
                if(rawFile.read(reinterpret_cast<char*>(&samples[channels*i]), 2) == 0)
                {
                    break;
                }
                for(int k = 1; k < channels; ++k)
                {
                    rawFile.read(reinterpret_cast<char*>(&samples[channels*i + k]), 2);
                }
            }
            else
            {
                if(rawFile.read(reinterpret_cast<char*>(&samples[i]), 2) == 0)
                {
                    break;
                }
                for(int k = 1; k < channels; ++k)
                {
                    rawFile.read(reinterpret_cast<char*>(&samples[k*pimpl->ctx->frame_size + i]), 2);
                }
            }
        }
        // channels, 2 bytes per sample, i samples read
        bytesLeft -= 2*channels*i;
        frame->nb_samples = i;
        frame->pts = av_rescale_q(samples_count, {1, pimpl->ctx->sample_rate}, pimpl->ctx->time_base);
        samples_count += i;

        // Try to encode the sample
        if(avcodec_encode_audio2(pimpl->ctx, pkt.get(), flush_output ? NULL : frame.get(), &got_output) < 0)
        {
            if(!(pimpl->oc->oformat->flags & AVFMT_NOFILE))
            {
                avio_close(pimpl->oc->pb);
            }
            throw std::runtime_error("Could not encode frame!");
        }
        // Check if there is data returned by the encoder
        if(!got_output)
        {
            if(flush_output)
            {
                audio_eof = true;
            }
        }
        else
        {
            // rescale output packet timestamp values from codec to stream timebase
            pkt->pts = av_rescale_q_rnd(pkt->pts, pimpl->ctx->time_base, pimpl->stream->time_base,
                                        static_cast<AVRounding>(AV_ROUND_NEAR_INF | AV_ROUND_PASS_MINMAX));
            pkt->dts = av_rescale_q_rnd(pkt->dts, pimpl->ctx->time_base, pimpl->stream->time_base,
                                        static_cast<AVRounding>(AV_ROUND_NEAR_INF | AV_ROUND_PASS_MINMAX));
            pkt->duration = av_rescale_q(pkt->duration, pimpl->ctx->time_base, pimpl->stream->time_base);
            pkt->stream_index = pimpl->stream->index;

            // Write compressed packet to file
            if(av_write_frame(pimpl->oc.get(), pkt.get()) < 0)
            {
                if(!(pimpl->oc->oformat->flags & AVFMT_NOFILE))
                {
                    avio_close(pimpl->oc->pb);
                }
                throw std::runtime_error("Error while writing audio frame!");
            }
        }
        // Signal progress
        emit signalProgress(bytesToRead - bytesLeft);
    }

    // Write trailer before closing codec
    av_write_trailer(pimpl->oc.get());

    if(!(pimpl->oc->oformat->flags & AVFMT_NOFILE))
    {
        avio_close(pimpl->oc->pb);
    }
}

void AudioEncoder::encode(const QString& rawFileName, const QString& outFileName,
                          std::vector<Marker>::const_iterator begin,
                          std::vector<Marker>::const_iterator end,
                          int sampleRate, int channels)
{
    pimpl->use_markers = true;
    pimpl->markersBegin = begin;
    pimpl->markersEnd = end;
    encode(rawFileName, outFileName, sampleRate, channels);
    pimpl->use_markers = false;
}
