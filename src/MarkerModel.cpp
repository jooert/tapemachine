/*
  Copyright (C) 2019  Johannes Oertel

  This program is free software: you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free Software
  Foundation, either version 3 of the License, or (at your option) any later
  version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
  details.

  You should have received a copy of the GNU General Public License along with
  this program. If not, see <https://www.gnu.org/licenses/>.
*/

#include "MarkerModel.hpp"

#include <QDebug>
#include <QTime>

QString Marker::bytesToTime(qint64 value)
{
    return QTime(0, 0, static_cast<float>(value)/(4*44100)).toString();
}

MarkerModel::MarkerModel(QObject *parent) :
    QAbstractTableModel(parent)
{
}

int MarkerModel::rowCount(const QModelIndex&) const
{
    return markers.size();
}

int MarkerModel::columnCount(const QModelIndex&) const
{
    return 3;
}

QVariant MarkerModel::data(const QModelIndex& index, int role) const
{
    if(role == Qt::DisplayRole)
    {
        if(index.row() < markers.size())
        {
            if(index.column() == 0)
            {
                return Marker::bytesToTime(markers[index.row()].begin);
            }
            if(index.column() == 1)
            {
                return Marker::bytesToTime(markers[index.row()].end);
            }
            if(index.column() == 2)
            {
                const Marker& m = markers[index.row()];
                return Marker::bytesToTime(m.end - m.begin);
            }
        }
    }

    return QVariant();
}

QVariant MarkerModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(role == Qt::DisplayRole)
    {
        if(orientation == Qt::Horizontal)
        {
            switch(section)
            {
            case 0:
                return QString("Beginn");
            case 1:
                return QString("Ende");
            case 2:
                return QString("Dauer");
            }
        }
    }

    return QVariant();
}

void MarkerModel::addMarker(Marker& m)
{
    int row = markers.size();
    beginInsertRows(QModelIndex(), row, row);
    markers.push_back(m);
    endInsertRows();
}

void MarkerModel::clear()
{
    beginResetModel();
    markers.clear();
    endResetModel();
}
