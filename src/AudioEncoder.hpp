/*
  Copyright (C) 2019  Johannes Oertel

  This program is free software: you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free Software
  Foundation, either version 3 of the License, or (at your option) any later
  version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
  details.

  You should have received a copy of the GNU General Public License along with
  this program. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef LIBAVPP_HPP
#define LIBAVPP_HPP

#include <memory>
#include <vector>

#include <QObject>
#include <QString>

#include "Marker.hpp"

class AudioEncoder : public QObject
{
    Q_OBJECT
public:
    explicit AudioEncoder(QObject* parent = 0, const QString& formatName = "mp3");
    ~AudioEncoder();

    // Encode a file containing raw audio samples
    void encode(const QString& rawFileName, const QString& outFileName,
                int sampleRate = 44100, int channels = 2);
    // Encode parts of a file containing raw audio samples
    void encode(const QString& rawFileName, const QString& outFileName,
                std::vector<Marker>::const_iterator begin,
                std::vector<Marker>::const_iterator end,
                int sampleRate = 44100, int channels = 2);

signals:
    // Signals for connecting a QProgressBar to watch the encoding process
    void signalProgress(int);
    void maxProgress(int);
    void minProgress(int);

private:
    struct impl;
    std::unique_ptr<impl> pimpl;
};

#endif // LIBAVPP_HPP
