/*
  Copyright (C) 2019  Johannes Oertel

  This program is free software: you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free Software
  Foundation, either version 3 of the License, or (at your option) any later
  version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
  details.

  You should have received a copy of the GNU General Public License along with
  this program. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QAudioInput>
#include <QFile>
#include <QFutureWatcher>
#include <QMainWindow>
#include <QProgressBar>
#include <QSettings>

#include "AudioEncoder.hpp"
#include "LevelMeter.hpp"
#include "MarkerModel.hpp"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void recordClicked(bool value);
    void markerClicked(bool value);
    void browseClicked();
    void inputDeviceChanged(int index);
    void updateClock();
    void showInfo();
    void showSettings();
    void volumeChanged(int value);
    void analyseBytes();

protected:
    void closeEvent(QCloseEvent* event);

public slots:
    void encodingFinished();

private:
    Ui::MainWindow *ui;
    QSettings settings;
    QProgressBar encodingProgressBar;
    MarkerModel markerModel;
    Marker currentMarker;
    QFile mainFile;
    QIODevice* audioInputDevice;
    QAudioInput* audioInput;
    QAudioFormat audioFormat;
    bool isRecording, isRendering, isEncodingParts;
    AudioEncoder encoder;
    QFutureWatcher<QString> watcher;
};

#endif // MAINWINDOW_HPP
