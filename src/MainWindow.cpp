/*
  Copyright (C) 2019  Johannes Oertel

  This program is free software: you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free Software
  Foundation, either version 3 of the License, or (at your option) any later
  version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
  details.

  You should have received a copy of the GNU General Public License along with
  this program. If not, see <https://www.gnu.org/licenses/>.
*/

#include "MainWindow.hpp"
#include "ui_MainWindow.h"

#include <cmath>

#include <QCloseEvent>
#include <QDate>
#include <QDebug>
#include <QDir>
#include <QFileDialog>
#include <QFileInfo>
#include <QFuture>
#include <QMessageBox>
#include <QTime>
#include <QtConcurrent>

#include "AudioEncoder.hpp"
#include "SettingsDialog.hpp"
#include "ui_SettingsDialog.h"
#include "Texts.hpp"
#include "Version.hpp"

static QString encodeFull(AudioEncoder* encoder, const QString& rawFileName, const QString& outFileName)
{
    try
    {
        encoder->encode(rawFileName, outFileName);
    }
    catch(std::runtime_error& e)
    {
        return e.what();
    }
    return QString();
}

static QString encodeParts(AudioEncoder* encoder, const QString& rawFileName,
			   const QString& outFileName,
			   std::vector<Marker>::const_iterator begin,
			   std::vector<Marker>::const_iterator end)
{
    try
    {
      encoder->encode(rawFileName, outFileName, begin, end);
    }
    catch(std::runtime_error& e)
    {
        return e.what();
    }
    return QString();
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    settings(QSettings::IniFormat, QSettings::UserScope, "Thomas-Gemeinde", "tapemachine"),
    markerModel(this),
    audioInput(NULL),
    isRecording(false),
    isRendering(false),
    isEncodingParts(false),
    encoder(),
    watcher()
{
    ui->setupUi(this);

    QString devname = settings.value("inputdevice").toString();
    // Fill QComboBox with available input audio devices
    for(auto d : QAudioDeviceInfo::availableDevices(QAudio::AudioInput))
    {
        ui->inputComboBox->addItem(d.deviceName(), QVariant::fromValue<QAudioDeviceInfo>(d));
        // Try to restore the device that was selected last time
        if(!devname.isEmpty() && d.deviceName() == devname)
        {
            ui->inputComboBox->setCurrentIndex(ui->inputComboBox->count() - 1);
        }
    }

    // Set last volume
    ui->volumeSlider->setValue(settings.value("volume", 100).toInt());

    // Initialize markerTableView
    ui->markerTableView->setModel(&markerModel);
    ui->markerTableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    // Add QProgressBar to status bar for showing encoding progress
    ui->statusBar->addPermanentWidget(&encodingProgressBar);

    // Initialize clock
    updateClock();

    // Set default values for filenames
    ui->directoryLineEdit->setText(settings.value("recordingsdir", QDir::homePath()).toString());
    ui->wholeRecordingLineEdit->setText(
                QDate::currentDate().toString(settings.value("wholeformatstring", "yyyy-MM-dd_'gottesdienst'").toString()));
    ui->partsRecordingLineEdit->setText(
                QDate::currentDate().toString(settings.value("partsformatstring", "yyyy-MM-dd_'predigt'").toString()));

    // Connect QProgressBar to encoding progress
    QObject::connect(&encoder, &AudioEncoder::minProgress, &encodingProgressBar, &QProgressBar::setMinimum);
    QObject::connect(&encoder, &AudioEncoder::maxProgress, &encodingProgressBar, &QProgressBar::setMaximum);
    QObject::connect(&encoder, &AudioEncoder::signalProgress, &encodingProgressBar, &QProgressBar::setValue);

    QObject::connect(&watcher, &QFutureWatcher<QString>::finished, this, &MainWindow::encodingFinished);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::recordClicked(bool value)
{
    if(value)
    {
        // Check if we have got a valid path
        QFileInfo recdirInfo(ui->directoryLineEdit->text());
        if(!recdirInfo.isDir())
        {
            qCritical() << ui->directoryLineEdit->text() << " ist kein Verzeichnis!";
            ui->recordButton->setChecked(false);
            return;
        }
        QDir recdir(ui->directoryLineEdit->text());

        // Prepare output file
        mainFile.setFileName(recdir.absoluteFilePath(ui->wholeRecordingLineEdit->text() + ".raw"));
        if(!mainFile.open(QIODevice::ReadWrite | QIODevice::Truncate))
        {
            qCritical() << "Datei konnte nicht geöffnet werden:" << mainFile.fileName();
            ui->recordButton->setChecked(false);
            return;
        }

        // Lock filename controls
        ui->fileOptionsGroupBox->setEnabled(false);
        // Lock input device combobox
        ui->inputComboBox->setEnabled(false);

        // Clear markers
        markerModel.clear();

        isRecording = true;
        ui->recordButton->setText("Stoppen");
    }
    else
    {
        // Stop recording
        isRecording = false;
        mainFile.close();

        isRendering = true;
        QDir recdir = QFileInfo(mainFile.fileName()).dir();

	// Encode whole recording
	ui->statusBar->showMessage("Speichere Aufnahme als MP3...");
	QFuture<QString> future = QtConcurrent::run(encodeFull, &encoder, mainFile.fileName(), recdir.absoluteFilePath(ui->wholeRecordingLineEdit->text() + ".mp3"));
	watcher.setFuture(future);
    }
}

void MainWindow::markerClicked(bool value)
{
    if(value)
    {
        currentMarker.begin = mainFile.pos();
        ui->markerButton->setText("Endmarker");
    }
    else
    {
        currentMarker.end = mainFile.pos();

        // Only add marker if it hasn't length zero
        if(currentMarker.end == currentMarker.begin)
        {
            ui->markerButton->setChecked(true);
            return;
        }

        // Add marker to model
        markerModel.addMarker(currentMarker);
        ui->markerButton->setText("Anfangsmarker");
    }
}

void MainWindow::browseClicked()
{
    QString dir = QFileDialog::getExistingDirectory(this, "Verzeichnis für Aufnahmen wählen", QDir::homePath());
    if(!dir.isEmpty())
    {
        ui->directoryLineEdit->setText(dir);
        settings.setValue("recordingsdir", dir);
    }
}

void MainWindow::inputDeviceChanged(int index)
{
    // Choose audio format
    QAudioFormat format;
    format.setSampleRate(44100);
    format.setChannelCount(2);
    format.setSampleSize(16);
    format.setSampleType(QAudioFormat::SignedInt);
    format.setCodec("audio/pcm");

    // Check if format is available
    QAudioDeviceInfo info = ui->inputComboBox->itemData(index).value<QAudioDeviceInfo>();
    if(!info.isFormatSupported(format))
    {
        format = info.nearestFormat(format);
        qWarning() << "Sampleformat nicht unterstützt, benutze nächstähnliches:\n"
                   << "Kanäle: " << format.channelCount() << ", "
                   << "Samplerate: " << format.sampleRate() << ", "
                   << "Samplegröße: " << format.sampleSize() << " bit"
                   << "Sampletyp: " << format.sampleType();
    }

    audioFormat = format;

    // Store audio device choice permanently
    settings.setValue("inputdevice", info.deviceName());

    // Open audio device for input
    if(audioInput)
    {
        audioInput->stop();
        delete audioInput;
    }
    audioInput = new QAudioInput(ui->inputComboBox->currentData().value<QAudioDeviceInfo>(), audioFormat, this);
    audioInput->setNotifyInterval(1000);
    connect(audioInput, &QAudioInput::notify, this, &MainWindow::updateClock);

    audioInputDevice = audioInput->start();
    if(audioInput->state() == QAudio::StoppedState)
    {
        qCritical() << "Aufnahmegerät konnte nicht geöffnet werden, Fehlercode: " << audioInput->error();
    }
    connect(audioInputDevice, &QIODevice::readyRead, this, &MainWindow::analyseBytes);
}

void MainWindow::updateClock()
{
    ui->clockLabel->setText(QString("<h1>%1</h1>").arg(Marker::bytesToTime(mainFile.pos())));
}

void MainWindow::showInfo()
{
    QMessageBox msgBox;
    msgBox.setText("TapeMachine ist eine einfache Aufnahme-Software");
    msgBox.setInformativeText(QString("TapeMachine Version: %1\n\n%2").arg(getAppVersion(), Texts::LICENSE()));
    msgBox.setIcon(QMessageBox::Information);
    msgBox.exec();
}

void MainWindow::showSettings()
{
    SettingsDialog settings_dialog;
    settings_dialog.ui->removeRawCheckBox->setChecked(settings.value("removeraw", true).toBool());
    settings_dialog.ui->wholeFormatStringLineEdit->setText(settings.value("wholeformatstring", "yyyy-MM-dd_'gottesdienst'").toString());
    settings_dialog.ui->partsFormatStringLineEdit->setText(settings.value("partsformatstring", "yyyy-MM-dd_'predigt'").toString());
    if(settings_dialog.exec() == QDialog::Accepted)
    {
        settings.setValue("removeraw", settings_dialog.ui->removeRawCheckBox->isChecked());
        settings.setValue("wholeformatstring", settings_dialog.ui->wholeFormatStringLineEdit->text());
        settings.setValue("partsformatstring", settings_dialog.ui->partsFormatStringLineEdit->text());
    }
}

void MainWindow::volumeChanged(int value)
{
    double vol = 0.01 * value;
    if(audioInput)
    {
        audioInput->setVolume(vol);
    }
    ui->volumeLabel->setText(QString::number(vol, 'f', 2));
    settings.setValue("volume", value);
}

void MainWindow::analyseBytes()
{
    const int buffersize = audioInput->bytesReady();
    char* const buffer = new char[buffersize];
    const char* const end = buffer + buffersize;
    audioInputDevice->read(buffer, buffersize);

    qreal peakLevelL = 0.0, peakLevelR = 0.0;
    qreal sumL = 0.0, sumR = 0.0;

    const char* it = buffer;
    while(it < end)
    {
        const qint16 valueL = *reinterpret_cast<const qint16*>(it);
        it += 2;
        const qint16 valueR = *reinterpret_cast<const qint16*>(it);
        it += 2;
        const qreal fracValueL = static_cast<qreal>(valueL)/32768;
        const qreal fracValueR = static_cast<qreal>(valueR)/32768;
        peakLevelL = qMax(peakLevelL, std::fabs(fracValueL));
        peakLevelR = qMax(peakLevelR, std::fabs(fracValueR));
        sumL += fracValueL * fracValueL;
        sumR += fracValueR * fracValueR;

    }

    const int numSamples = buffersize/2;
    qreal rmsLevelL = sqrt(sumL/numSamples);
    rmsLevelL = qMax(qreal(0.0), rmsLevelL);
    rmsLevelL = qMin(qreal(1.0), rmsLevelL);
    qreal rmsLevelR = sqrt(sumR/numSamples);
    rmsLevelR = qMax(qreal(0.0), rmsLevelR);
    rmsLevelR = qMin(qreal(1.0), rmsLevelR);
    ui->levelMeterL->levelChanged(rmsLevelL, peakLevelL, numSamples);
    ui->levelMeterR->levelChanged(rmsLevelR, peakLevelR, numSamples);

    if(isRecording)
    {
        mainFile.write(buffer, buffersize);
    }
    delete[] buffer;
}

void MainWindow::closeEvent(QCloseEvent* event)
{
    if(isRecording || isRendering)
    {
        QMessageBox msgBox;
        msgBox.setText("Aufnahme läuft noch.");
        msgBox.setInformativeText("Beende zuerst die Aufnahme.");
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.exec();
        event->ignore();
    }
    else
    {
        event->accept();
    }
}

void MainWindow::encodingFinished()
{
    QString result = watcher.result();
    if(!result.isEmpty())
    {
        QMessageBox msgBox;
	msgBox.setText("Fehler beim Encodieren!");
	msgBox.setInformativeText(result);
	msgBox.setIcon(QMessageBox::Critical);
	msgBox.exec();
    }
    else if(!isEncodingParts)
    {
    	// Encode parts
	ui->statusBar->showMessage("Speichere markierte Teile als MP3...");
	QDir recdir = QFileInfo(mainFile.fileName()).dir();
	QFuture<QString> future =
	  QtConcurrent::run(encodeParts, &encoder, mainFile.fileName(),
			    recdir.absoluteFilePath(ui->partsRecordingLineEdit->text() + ".mp3"),
			    markerModel.markersBegin(), markerModel.markersEnd());
	watcher.setFuture(future);
	isEncodingParts = true;
	return;
    }
    else
    {
	// Only remove raw file when option is set and encoding was successful.
	if(settings.value("removeraw", true).toBool())
	{
	    QFile(mainFile.fileName()).remove();
	}
    }

    ui->statusBar->clearMessage();
    encodingProgressBar.setValue(0);

    // Unlock filename controls
    ui->fileOptionsGroupBox->setEnabled(true);
    // Unlock input device combobox
    ui->inputComboBox->setEnabled(true);

    isEncodingParts = false;
    isRendering = false;
    ui->recordButton->setText("Aufnehmen");
}
