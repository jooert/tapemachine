/*
  Copyright (C) 2019  Johannes Oertel

  This program is free software: you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free Software
  Foundation, either version 3 of the License, or (at your option) any later
  version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
  details.

  You should have received a copy of the GNU General Public License along with
  this program. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef LEVELMETER_HPP
#define LEVELMETER_HPP

#include <QTime>
#include <QWidget>

class LevelMeter : public QWidget
{
    Q_OBJECT

public:
    explicit LevelMeter(QWidget *parent = 0);
    ~LevelMeter();

    void paintEvent(QPaintEvent *event);
    void levelChanged(qreal rmsLevel, qreal peakLevel, int numSamples);

public slots:
    void reset();

private slots:
    void redrawTimerExpired();

private:
    qreal m_rmsLevel;
    qreal m_peakLevel;
    qreal m_decayedPeakLevel;

    QTime m_peakLevelChanged;
    QTime m_peakHoldLevelChanged;
    qreal m_peakDecayRate;
    qreal m_peakHoldLevel;

    QTimer *m_redrawTimer;

    QColor m_peakDecayedColor;
    QColor m_peakColor;

};

#endif // LEVELMETER_HPP
