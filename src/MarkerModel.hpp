/*
  Copyright (C) 2019  Johannes Oertel

  This program is free software: you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free Software
  Foundation, either version 3 of the License, or (at your option) any later
  version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
  details.

  You should have received a copy of the GNU General Public License along with
  this program. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MARKERMODEL_HPP
#define MARKERMODEL_HPP

#include <vector>

#include <QAbstractTableModel>

#include "Marker.hpp"

class MarkerModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit MarkerModel(QObject *parent = 0);

    int rowCount(const QModelIndex& = QModelIndex()) const;
    int columnCount(const QModelIndex& = QModelIndex()) const;
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;

    void addMarker(Marker& m);
    std::vector<Marker>::const_iterator markersBegin() const { return markers.begin(); }
    std::vector<Marker>::const_iterator markersEnd() const { return markers.end(); }
    void clear();

private:
    std::vector<Marker> markers;

};

#endif // MARKERMODEL_HPP
